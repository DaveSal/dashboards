class AddDashboardTypeToDashboards < ActiveRecord::Migration[5.2]
  def change
    add_reference :dashboards, :dashboard_type, foreign_key: true
  end
end
