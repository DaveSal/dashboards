namespace :db do
  namespace :data do
    task :migrate_dashboard_types => [:environment] do
      print "******Migrating dashboard types "

      DashboardType.delete_all

      File.readlines("#{Rails.root}/db/dashboard_types.txt").each do |title|
        own_type = DashboardType.create!(title: title.strip)
        print "."
      rescue StandardError => e
        puts "Cannot create dashboard type due to: #{e}"
      end
      puts "done!"
    end
  end
end