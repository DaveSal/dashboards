class Dashboard < ApplicationRecord
  belongs_to :dashboard_type
  belongs_to :user

  validates :content, presence: true

  before_validation :set_dashboard_index

  scope :by_user,
        -> (user_id) { where(user_id: user_id) }

  class << self
    def update_order(dashboard_order_params)
      dashboard_order_params.each do |id, index|
        dashboard = Dashboard.find_by_id(id)
        next if dashboard.nil?
        dashboard.update_column(:index, index)
      end
    end
  end

  private

    def set_dashboard_index
      self.index = Dashboard.by_user(self.user_id).count + 1
    end
end
