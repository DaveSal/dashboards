class DashboardType < ApplicationRecord
  has_many :dashboards

  validates               :title, presence: true
  validates_uniqueness_of :title
end
