document.addEventListener("DOMContentLoaded", function(event) {
  // Making dashboards sortable

  let dashboardsList = document.querySelector("#dashboardsList");

  if (dashboardsList) {
    Sortable.create(dashboardsList, {
      dataIdAttr: 'data-id',
      animation: 1000,
    });
  }

  // Collecting dashboards indexes with order

  let saveOrderButton = document.querySelector("#saveDashboardOrder");

  if (saveOrderButton) {
    saveOrderButton.addEventListener("click", function(event) {
      event.preventDefault();

      let dashboardOrder = {};

      document.querySelectorAll(".dashboard").forEach(function(el, i) {
        dashboardOrder[el.dataset.id] = (i + 1);
      });

      // Showing that I know jquery syntax too =) To be honest, Ajax request with jquery are more simple
      $.ajax({
        url: "/dashboards/update_order",
        dataType: "json",
        type: "post",
        contentType: "application/json",
        data: JSON.stringify({ "dashboards": dashboardOrder }),
        processData: false,
        success: function( data ){
          alert("Current dashboard ordering successfully saved!");
        },
        error: function( request ){
          alert("Errors: "+JSON.stringify(request));
        }
      });
    });
  }
});