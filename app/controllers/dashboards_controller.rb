class DashboardsController < ApplicationController
  before_action      :authenticate_user!
  skip_before_action :verify_authenticity_token, only: [:update_order]

  def new
    @dashboard = current_user.dashboards.build
  end

  def create
    @dashboard = current_user.dashboards.build(dashboard_params)

    if @dashboard.save
      flash[:success] = "New dashboard successfully created!"
      redirect_to root_path
    else
      flash.now[:error] = "You made some mistakes within creating a dashboard."
      render "new"
    end
  end

  def update_order
    Dashboard.update_order(dashboard_order_params)
  end

  private

    def dashboard_params
      params.require(:dashboard).permit(:content, :dashboard_type_id)
    end

    def dashboard_order_params
      params.require(:dashboards)
    end
end
