class PagesController < ApplicationController
  def homepage
    @dashboards = current_user.dashboards.order("index ASC") if current_user
  end
end
