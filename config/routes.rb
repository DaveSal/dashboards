Rails.application.routes.draw do
  devise_for :users
  root "pages#homepage"

  resources :pages, only: [:homepage]
  resources :dashboards, only: [:new, :create] do
    post "update_order", on: :collection
  end
end
