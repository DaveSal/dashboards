require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#flash_class" do
    it "returns correct classes" do
      expect(helper.flash_class(:notice)).to eq("alert alert-info")
      expect(helper.flash_class(:success)).to eq("alert alert-success")
      expect(helper.flash_class(:error)).to eq("alert alert-danger")
      expect(helper.flash_class(:alert)).to eq("alert alert-warning")
    end
  end
end
