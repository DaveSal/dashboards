FactoryBot.define do
  factory :dashboard do
    content { "Some content" }
    sequence(:index) { |i|  "#{i}" }

    association(:dashboard_type)
  end
end