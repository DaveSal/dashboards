FactoryBot.define do
  factory :dashboard_type do
    sequence(:title) { |i| "Title #{i}" }
  end
end
