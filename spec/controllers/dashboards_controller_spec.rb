require 'rails_helper'

RSpec.describe DashboardsController, type: :controller do
  before(:each) do
    user = create(:user, email: "user1@example.com")
    allow(controller).to receive(:current_user).and_return(user)
  end

  xdescribe "#new" do
    it "renders the new template" do
      get :new
      expect(response).to render_template("new")
    end

    it "initializes a new dashboard" do
      get :new
      expect(assigns(:dashboard)).to be_a_new(Dashboard)
    end
  end

  xdescribe "#create" do
    context "when dashboard params are correct" do
      before { @dashboard_type = create(:dashboard_type) }
      subject { post :create, params: { dashboard: FactoryBot.attributes_for(:dashboard).merge({ dashboard_type_id: @dashboard_type.id })} }

      it "creates a dashboard" do
        expect { subject }.to change(Dashboard, :count).by(1)
      end

      it "shows a success message" do
        expect(subject.request.flash[:success]).not_to be_nil
      end

      it "redirect to a root path" do
        expect(subject.request).to redirect_to(root_path)
      end
    end

    context "when dashboard params are incorrect" do
      subject { post :create, params: { dashboard: FactoryBot.attributes_for(:dashboard).merge({ content: nil })} }

      it "doesn't create a dashboard" do
        expect { subject }.to change(Dashboard, :count).by(0)
      end

      it "shows an error message" do
        expect(subject.request.flash[:error]).not_to be_nil
      end
    end
  end

  xdescribe "#update_order" do
    it "calls `update_order` method on `Dashboard` model" do
      post :update_order

      expect(Dashboard).to receive(:update_order).once
    end
  end
end
