require 'rails_helper'

RSpec.describe PagesController, type: :controller do
  describe "#homepage" do
    it "renders the homepage template" do
      get :homepage
      expect(response).to render_template("homepage")
    end
  end
end
