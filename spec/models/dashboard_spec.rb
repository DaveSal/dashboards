require 'rails_helper'

RSpec.describe Dashboard, type: :model do
  it { is_expected.to validate_presence_of(:content) }

  xdescribe "#by_user" do
    before do
      @user = create(:user, email: "test1@example.com")
      @another_user = create(:user, email: "test2@example.com")

      @user_dashboards = create_list(:dashboard, 2, user: @user)
      @another_user_dashboards = create_list(:dashboard, 3, user: @another_user)
    end

    it "returns only specified user's dashboards" do
      expect(Dashboard.by_user(@user.id)).to eq(@user_dashboards)
      expect(Dashboard.by_user(@user.id).count).to eq(2)
    end
  end
end
